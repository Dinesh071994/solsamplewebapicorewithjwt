﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using solSampleWebApiCoreWithJWT.Models;

namespace solSampleWebApiCoreWithJWT.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly MyAppContext _context;

        public EmployeeController(MyAppContext context)
        {
            _context = context;
        }
         
        [HttpGet("employee/summary")]
        public ActionResult<List<Employee>> GetEmployees()
        {
            var query = _context.Employee.AsQueryable();
            List<Employee> employees = query.ToList();
            return Ok(employees);
        }
    }
}

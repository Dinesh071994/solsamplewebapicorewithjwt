﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using solSampleWebApiCoreWithJWT.Models;

namespace solSampleWebApiCoreWithJWT.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly MyAppContext _context;

        public InventoryController(MyAppContext context)
        {
            _context = context;
        }
         
        [HttpGet("products/summary")]
        public ActionResult<List<Products>> GetProducts(string category)
        {
            var query = _context.Products.AsQueryable();
            List<Products> products = query.ToList();
            if (products == null || products.Count == 0)
            {
                return NotFound();
            }
            if (!string.IsNullOrEmpty(category))
            {
                products =products.FindAll(i => string.Equals(i.Category, category, StringComparison.OrdinalIgnoreCase));
            }

            return Ok(products);
        }
    }
}

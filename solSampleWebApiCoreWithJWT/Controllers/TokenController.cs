﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using solSampleWebApiCoreWithJWT.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace solSampleWebApiCoreWithJWT.Controllers
{
    /// <summary>
    /// Token controller
    /// </summary>
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        public IConfiguration _configuration;

        /// <summary>
        /// Token Controller constructor
        /// </summary>
        /// <param name="config"></param>
        public TokenController(IConfiguration config)
        {
            _configuration = config;
        }

        #region Post
        [HttpPost("token")]
        public IActionResult Post(LoginRequest _userData)
        {
            if (_userData != null && _userData.UserName != null && _userData.Password != null)
            {
                var user = GetUser(_userData.UserName, _userData.Password);

                if (user != null)
                {
                    //create claims details based on the user information
                    var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                   };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                        _configuration["Jwt:Audience"],
                        claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    return BadRequest("Invalid credentials");
                }
            }
            else
            {
                return BadRequest();
            }
        }
        #endregion


        #region GetUser
        private LoginRequest GetUser(string userName, string password)
        {
            List<LoginRequest> users = new List<LoginRequest>
            {
                new LoginRequest
                {
                    UserName = "DineshM",
                    Password  = "test"
                },
                new LoginRequest
                {
                    UserName = "DineshK",
                    Password  = "test2"
                }
            };
            return users.FirstOrDefault(u => u.UserName == userName && u.Password == password);
        }
        #endregion

    }
}
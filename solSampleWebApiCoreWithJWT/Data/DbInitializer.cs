﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace solSampleWebApiCoreWithJWT.Models
{
    public static class DbInitializer
    {
        public static void Initialize(MyAppContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Products.Any() && context.Employee.Any())
            {
                return;   // DB has been seeded
            }

            var products = new List<Products>
            {
                new Products
                {
                    Category = "Mobiles",
                    Name = "IPhone11",
                    Color = "Black",
                    UnitPrice = 62000,
                     AvailableQuantity = 50
                },
                new Products
                {
                    Category = "Mobiles",
                    Name = "IPhone11",
                    Color = "White",
                    UnitPrice = 63000,
                     AvailableQuantity = 50
                },
                new Products
                {
                    Category = "Tab",
                    Name = "SamsungA4",
                    Color = "White",
                    UnitPrice = 20000,
                     AvailableQuantity = 10
                },

            };

            context.Products.AddRange(products);
            context.SaveChanges();

            var companies = new List<Company>
            {
                new Company
                {
                         Name = "Conduent",
                },
                 new Company
                {
                    Name = "Infosys",
                },
            };

            context.Company.AddRange(companies);
            context.SaveChanges();

            var employees = new List<Employee>
            {
                new Employee
                {
                   FirstName = "Dinesh",
                   LastName  = "Mahenderkar",
                   Age = 26,
                   Company = companies.FirstOrDefault(x=> x.Name == "Conduent")
                },
                 new Employee
                {
                   FirstName = "Srikanth",
                   LastName  = "Mahenderkar",
                   Age = 26,
                   Company = companies.FirstOrDefault(x=> x.Name == "Infosys")
                },
            };

            context.Employee.AddRange(employees);
            context.SaveChanges();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace solSampleWebApiCoreWithJWT.Models
{
    public class MyAppContext : DbContext
    {
        public MyAppContext(DbContextOptions<MyAppContext> options) : base(options)
        {
        }

        public virtual DbSet<Products> Products { get; set; }

        public virtual DbSet<Employee> Employee { get; set; }

        public virtual DbSet<Company> Company { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("Employee");
            modelBuilder.Entity<Company>().ToTable("Company");

            modelBuilder.Entity<Company>()
               .HasKey(c => c.Id).HasName("PrimaryKey_CompanyId");
            modelBuilder.Entity<Employee>()
              .HasKey(c => c.Id).HasName("PrimaryKey_EmployeeId");

            modelBuilder.Entity<Products>()
                .HasKey(c => c.ProductId).HasName("PrimaryKey_ProductId");

            modelBuilder.Entity<Company>().HasMany(c => c.Employees).WithOne(e => e.Company);
        }

    }
}